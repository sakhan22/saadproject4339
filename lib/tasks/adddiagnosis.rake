require 'csv'

namespace :csv do

  desc 'Import CSV Data'
  task :import_diagnosis => :environment do

    csv_file_path = '/diasgnose.csv'

    CSV.foreach(csv_file_path) do |row|
      Diagnosis.create!({
                        :code => row[0],
                        :description => row[1],
                        :price => row[2],

                    })
      puts 'Row added!'
    end
  end
end