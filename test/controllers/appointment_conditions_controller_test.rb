require 'test_helper'

class AppointmentConditionsControllerTest < ActionController::TestCase
  setup do
    @appointment_condition = appointment_conditions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:appointment_conditions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create appointment_condition" do
    assert_difference('AppointmentCondition.count') do
      post :create, appointment_condition: { charge_fee: @appointment_condition.charge_fee, current_status: @appointment_condition.current_status }
    end

    assert_redirected_to appointment_condition_path(assigns(:appointment_condition))
  end

  test "should show appointment_condition" do
    get :show, id: @appointment_condition
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @appointment_condition
    assert_response :success
  end

  test "should update appointment_condition" do
    patch :update, id: @appointment_condition, appointment_condition: { charge_fee: @appointment_condition.charge_fee, current_status: @appointment_condition.current_status }
    assert_redirected_to appointment_condition_path(assigns(:appointment_condition))
  end

  test "should destroy appointment_condition" do
    assert_difference('AppointmentCondition.count', -1) do
      delete :destroy, id: @appointment_condition
    end

    assert_redirected_to appointment_conditions_path
  end
end
