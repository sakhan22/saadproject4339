# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



AppointmentCondition.create([{current_status: 'Created', charge_fee: 0},
                          {current_status: 'Completed', charge_fee: 30},
                          {current_status: 'Canceled', charge_fee: 50} ])

TimeSlot.create([{time_name: '8:30 am', time_time: '08:30'},
                 {time_name: '9:00 am', time_time: '09:00'},
                 {time_name: '9:30 am', time_time: '09:30'},
                 {time_name: '10:00 am', time_time: '10:00'},
                 {time_name: '10:30 am', time_time: '10:30'},
                 {time_name: '11:00 am', time_time: '11:00'},
                 {time_name: '11:30 am', time_time: '11:30'},
                 {time_name: '12:00 pm', time_time: '12:00'},
                 {time_name: '12:30 PM', time_time: '12:30'},
                 {time_name: '1:00 pm', time_time: '13:00'},
                 {time_name: '1:30 PM', time_time: '13:30'},
                 {time_name: '2:00 pm', time_time: '14:00'},
                 {time_name: '2:30 PM', time_time: '14:30'},
                 {time_name: '3:00 pm', time_time: '15:00'},
                 {time_name: '3:30 pm', time_time: '15:30'},
                 {time_name: '4:00 PM', time_time: '16:00'},
                 {time_name: '4:30 pm', time_time: '16:30'}])

Doctor.create([{dlast_name: 'Hugo', dfirst_name: 'Doctor', doc_phone: '832-293-0349'},
               {dlast_name: 'Frankinstien', dfirst_name: 'Doctor', doc_phone: '832-567-1234'},
               {dlast_name: 'Patel', dfirst_name: 'Shah', doc_phone: '713-285-1954'},
               {dlast_name: 'Smith', dfirst_name: 'John', doc_phone: '681-534-8730'}])

Patient.create([{last_name: 'Khan', first_name: 'Saad', phone: '832-563-0359'},
                {last_name: 'Petcov', first_name: 'Dimitri', phone: '832-785-134'},
                {last_name: 'Phobia', first_name: 'Xeno', phone: '713-754-8633'},
                {last_name: 'Smith', first_name: 'John', phone: '281-743-7749'}])