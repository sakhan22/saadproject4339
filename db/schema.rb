# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141130230333) do

  create_table "appointment_conditions", force: true do |t|
    t.string   "current_status"
    t.float    "charge_fee"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appointment_statuses", force: true do |t|
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appointments", force: true do |t|
    t.integer  "patient_id"
    t.integer  "doctor_id"
    t.date     "visit_date"
    t.integer  "time_slot_id"
    t.text     "patient_notes"
    t.integer  "diagnosis_id"
    t.text     "doctor_notes"
    t.integer  "appointment_condition_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "diagnoses", force: true do |t|
    t.string   "code"
    t.string   "description"
    t.decimal  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "doctors", force: true do |t|
    t.string   "dlast_name"
    t.string   "dfirst_name"
    t.string   "doc_phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patients", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "time_slots", force: true do |t|
    t.string   "time_name"
    t.time     "time_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
