class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :doctor_id
      t.date :visit_date
      t.integer :time_slot_id
      t.text :patient_notes
      t.integer :diagnosis_id
      t.text :doctor_notes
      t.integer :appointment_status_id

      t.timestamps
    end
  end
end
