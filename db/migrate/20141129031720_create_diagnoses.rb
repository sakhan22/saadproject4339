class CreateDiagnoses < ActiveRecord::Migration
  def change
    create_table :diagnoses do |t|
      t.string :code
      t.string :description
      t.decimal :price

      t.timestamps
    end
  end
end
