class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :dlast_name
      t.string :dfirst_name
      t.string :doc_phone

      t.timestamps
    end
  end
end
