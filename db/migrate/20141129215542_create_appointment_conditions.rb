class CreateAppointmentConditions < ActiveRecord::Migration
  def change
    create_table :appointment_conditions do |t|
      t.string :current_status
      t.float :charge_fee

      t.timestamps
    end
  end
end
