class RenameStatusToCondition < ActiveRecord::Migration
  def change
    rename_column :appointments, :appointment_status_id, :appointment_condition_id
  end
end
