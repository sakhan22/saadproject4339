Rails.application.routes.draw do


  get 'worker_invoice/invoiced'

  get 'doctor_appoint/ready'

  get 'admin_done/doned'

  get 'admin_appointment/completed'

  get 'patient_appointment/edit'

  resources :appointment_conditions

  resources :time_slots

  resources :appointments

  resources :timeslots

  resources :appointment_statuses

  resources :diagnoses

  resources :doctors

  resources :patients

  get 'worker_invoice/geninvoice'

  get 'doctor_appointment/docsession'


  get 'admin_edit_db/editdatabase'

  get 'worker_dashboard/workerdash'

  get 'doctor_dashboard/doctordash'

  get 'patient_dashboard/patientdash'

  get 'admin_dashboard/admindash'

  get 'sysadmin/admin'

  get 'dashboard/dash'

  get 'controllername/new'

  get 'controllername/create'

  get 'admin_done/doned'




  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'dashboard#dash'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
