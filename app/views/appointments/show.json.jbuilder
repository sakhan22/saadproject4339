json.extract! @appointment, :id, :patient_id, :doctor_id, :visit_date, :time_slot_id, :patient_notes, :diagnosis_id, :doctor_notes, :appointment_status_id, :created_at, :updated_at
