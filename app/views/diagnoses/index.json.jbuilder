json.array!(@diagnoses) do |diagnosis|
  json.extract! diagnosis, :id, :code, :description, :price
  json.url diagnosis_url(diagnosis, format: :json)
end
