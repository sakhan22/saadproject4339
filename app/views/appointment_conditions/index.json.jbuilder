json.array!(@appointment_conditions) do |appointment_condition|
  json.extract! appointment_condition, :id, :current_status, :charge_fee
  json.url appointment_condition_url(appointment_condition, format: :json)
end
