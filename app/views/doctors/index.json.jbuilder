json.array!(@doctors) do |doctor|
  json.extract! doctor, :id, :dlast_name, :dfirst_name, :doc_phone
  json.url doctor_url(doctor, format: :json)
end
