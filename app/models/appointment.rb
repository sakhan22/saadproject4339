class Appointment < ActiveRecord::Base
  #has_many :patients
 # has_many :doctors
 # has_many :time_slots
  #has_many :diagnosises
 # has_many :appointment_conditions
  belongs_to :patient
  belongs_to :doctor
  belongs_to :time_slot
  belongs_to :diagnosis
  belongs_to :appointment_condition


  validates_uniqueness_of :doctor_id, scope: [:time_slot_id, :visit_date], :message=>"Appointment booked, please select different time and/or doctor"

  validates_presence_of :patient_id
  validates_presence_of :doctor_id
  validates_presence_of :time_slot_id
  validates_presence_of :diagnosis_id
  validates_presence_of :appointment_condition_id

  validate :future
  validate :not_weekend
  before_destroy :ateHrs

  def future
    if Date.today >= self.visit_date
    errors.add :visit_date, "You must select date after today"
    end
  end


  def not_weekend
    if visit_date.saturday? == true or visit_date.sunday? == true
      errors.add :visit_date, ': Dermo is closed on weekends, call 911 for emergency'
      return false
    end
  end

  def ateHrs
      if (time_slot.time_time - Time.now) < 8
      errors.add :base, "You must call front desk at 832-949-9867 to cancel or reschedule if 8 hours before appointment"
      return false
      end
  end
  def self.appointmentNew
    Appointment.where("appointment_condition_id =?", 1)
  end
  def self.appointmentDone
    Appointment.where("appointment_condition_id =?", 2)
  end



  def self.patient_create
    @appointment = Appointment.new
    end



end
