class Patient < ActiveRecord::Base
 has_many :appointments
  #belongs_to :appointment
  has_many :doctors, :through => :appointments
  has_many :time_slots, :through => :appointments
 has_many :diagnosises, :through => :appointments

  validates_presence_of :first_name
  validates_presence_of :last_name





  def full_name
    "#{last_name}, #{first_name}"
  end

end