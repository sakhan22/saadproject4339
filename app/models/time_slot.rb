class TimeSlot < ActiveRecord::Base
  has_many :appointments

  validates_presence_of :time_time
  validates_presence_of :time_name

end
