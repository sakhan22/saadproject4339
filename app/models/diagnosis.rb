class Diagnosis < ActiveRecord::Base
  has_many :appointments
  #belongs_to :appointment

  validates_presence_of :code
  validates_presence_of :description
  validates_presence_of :price

  def diagcode
    code + ': ' + description
  end
end
