class Doctor < ActiveRecord::Base
 has_many :appointments
  #belongs_to :appointment
  has_many :patients, through: :appointments

  validates_presence_of :dfirst_name
  validates_presence_of :dlast_name


  def full_name
    "#{dlast_name}, #{dfirst_name}"
  end
end
