class AppointmentConditionsController < ApplicationController
  before_action :set_appointment_condition, only: [:show, :edit, :update, :destroy]

  # GET /appointment_conditions
  # GET /appointment_conditions.json
  def index
    @appointment_conditions = AppointmentCondition.all
  end

  # GET /appointment_conditions/1
  # GET /appointment_conditions/1.json
  def show
  end

  # GET /appointment_conditions/new
  def new
    @appointment_condition = AppointmentCondition.new
  end

  # GET /appointment_conditions/1/edit
  def edit
  end

  # POST /appointment_conditions
  # POST /appointment_conditions.json
  def create
    @appointment_condition = AppointmentCondition.new(appointment_condition_params)

    respond_to do |format|
      if @appointment_condition.save
        format.html { redirect_to @appointment_condition, notice: 'Appointment condition was successfully created.' }
        format.json { render :show, status: :created, location: @appointment_condition }
      else
        format.html { render :new }
        format.json { render json: @appointment_condition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /appointment_conditions/1
  # PATCH/PUT /appointment_conditions/1.json
  def update
    respond_to do |format|
      if @appointment_condition.update(appointment_condition_params)
        format.html { redirect_to @appointment_condition, notice: 'Appointment condition was successfully updated.' }
        format.json { render :show, status: :ok, location: @appointment_condition }
      else
        format.html { render :edit }
        format.json { render json: @appointment_condition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appointment_conditions/1
  # DELETE /appointment_conditions/1.json
  def destroy
    @appointment_condition.destroy
    respond_to do |format|
      format.html { redirect_to appointment_conditions_url, notice: 'Appointment condition was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appointment_condition
      @appointment_condition = AppointmentCondition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appointment_condition_params
      params.require(:appointment_condition).permit(:current_status, :charge_fee)
    end
end
